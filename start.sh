#!/bin/bash
. ./start-environment.sh
# Run the new image. Automatically goes to DAEMON if the second parameter is not given
echo "Container is started in $RUN_MODE"
docker run $PARAMETERS --name postgresql_server_$1 \
    -v $PWD/postgres-$1/:/data/ \
    -p $PORT:5432 \
    sicflex/postgresql:$1 $3
