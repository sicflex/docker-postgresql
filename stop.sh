#!/bin/bash
if [ ! -z "$1" ]; then
    docker exec postgresql_server_$1 "/usr/sbin/service postgresql stop; /bin/sleep 10; ps aux | grep sh | awk {'print $2'} | xargs kill"
    docker stop postgresql_server_$1
    docker rm postgresql_server_$1
    echo "Docker container postgresql_server_$1 has been stopped"
else
    echo "Version should be defined.For example:"
    echo "Syntax is:"
    echo "   $0 postgresql-version"
    echo ""
    echo "Example:"
    echo "#> $0 9.4"
    echo "   Stops and release container running with version 9.4"
fi
