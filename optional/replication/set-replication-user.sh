#!/bin/bash

# Creates the replication user
sudo -u postgres createuser -s repmgr
sudo -u postgres createdb -O repmgr repmgr
sudo -u postgres repmgr master register
