#!/bin/bash
function updateConfiguration() {
    mkdir -p /etc/postgresql/${POSTGRESQL}/main/conf.d
    for z in /data/main/*; do
        echo "Copying configuration file $z to /etc/postgresql/${POSTGRESQL}/main/"
        cp "$z" /etc/postgresql/${POSTGRESQL}/main/
    done
    for z in /data/conf.d/*.conf; do
        echo "Copying additional postgresql configuration $z to /etc/postgresql/${POSTGRESQL}/main/conf.d/"
        cp "$z" /etc/postgresql/${POSTGRESQL}/main/conf.d/
    done
    echo "include_dir = 'conf.d'" >> /etc/postgresql/${POSTGRESQL}/main/postgresql.conf
}

# Creates the data cluster if required
if [ ! -e "/data/pgdata" ]; then
    pg_dropcluster --stop ${POSTGRESQL} main
    pg_createcluster --locale en_US.UTF8 -d /data/pgdata ${POSTGRESQL} main

    updateConfiguration

    service postgresql start && \
    sleep 20
    for z in /data/init-sqls/*.sql; do
        sudo -u postgres psql -f "$z"
    done
else
    updateConfiguration
    service postgresql start
fi

while true
do
    sleep 1
done

