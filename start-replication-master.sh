#!/bin/bash
if [ ! -z "$1" ]; then
# If we are building a NEW postgresql image
    if [ ! -f "Dockerfile-$1" ]; then
        sed "s/{postgresql_version}/$1/g;s/{refreshed_date}/`date +%Y-%m-%d:%H:%M`/g" Dockerfile-template > Dockerfile-$1
    fi

    mkdir -p postgres-$1
    if [ ! -e "postgres-$1/main/" ]; then rsync -a --progress main/* postgres-$1/main/; fi
    if [ ! -e "postgres-$1/conf.d/" ]; then rsync -a --progress conf.d/* postgres-$1/conf.d/; fi
    if [ ! -e "postgres-$1/init-sqls/" ]; then rsync -a --progress init-sqls/* postgres-$1/init-sqls/; fi
    if [ ! -e "postgres-$1/shell-scripts/" ]; then rsync -a --progress shell-scripts/* postgres-$1/shell-scripts/; fi

# Build or update the new image
    docker build -t sicflex/postgresql:$1 -f Dockerfile-$1 .
    if [ -z "$2" ]; then
        PORT=5432
    else
        PORT=$2
    fi
    if [ -z "$3" ]; then
        PARAMETERS=-d
    else
        PARAMETERS=-ti --rm
    fi

# Run the new image. Automatically goes to DAEMON if the second parameter is not given
    docker run $PARAMETERS --name postgresql_server_$1 \
        -v $PWD/postgres-$1/:/data/ \
        -p $PORT:5432 \
        sicflex/postgresql:$1 $3
else
    echo "Version should be defined.For example:"
    echo "Syntax is:"
    echo "   $0 postgresql-version [port [command]]"
    echo ""
    echo "Where:"
    echo "  postgresql-version: Can be 9.1 through 9.6"
    echo "                port: Connection port"
    echo "             command: Is a shell command (such as /bin/bash, for example) if specified"
    echo "                      the container will open a terminal in interactive mode and runs the"
    echo "                      passed command upon start up. If command is not specified the container"
    echo "                      will run the scripts found in shell-script-n.n"
    echo ""
    echo "Examples:"
    echo "#> $0 9.4"
    echo "   Builds a 9.4 image version of postgresql and runs it as a daemon (like a service)"
    echo ""
    echo "#> $0 9.6 5435 /bin/bash"
    echo "   Builds a 9.6 postgresql image, runs it opening a terminal in interactive mode within a bash session. Uses port 5435 for external access"
fi
