alter user postgres with password 'm1P31R8';
create user sicflex with superuser password 'm1P31R8';
create user lportal with password 'lportal';
create user wubiq with password 'wubiq';
create user wiki with password 'wiki';

create database sicflex with owner sicflex;
create database lportal with owner lportal;
create database wubiq with owner wubiq;
create database wiki with owner wiki;

